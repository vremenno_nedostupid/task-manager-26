package ru.fedun.tm.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.fedun.tm.entity.User;
import ru.fedun.tm.enumerated.Role;
import ru.fedun.tm.exception.empty.EmptyEmailException;
import ru.fedun.tm.exception.empty.EmptyIdException;
import ru.fedun.tm.exception.empty.EmptyLoginException;
import ru.fedun.tm.exception.empty.EmptyPassException;
import ru.fedun.tm.exception.user.AccessDeniedException;
import ru.fedun.tm.marker.UnitServerCategory;
import ru.fedun.tm.repository.UserRepository;
import ru.fedun.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@Category(UnitServerCategory.class)
public class UserServiceTest {
/*

    private UserService userService;

    @Before
    public void initService() {
        userService = new UserService(new UserRepository());
    }

    @Test
    public void findByIdTest() {
        final User user = userService.create("user", "test", "Ivan", "Ivanov");
        final String userId = user.getId();
        assertEquals(user, userService.findById(userId));
    }

    @Test(expected = EmptyIdException.class)
    public void findByIdTestEmptyIdException() {
        userService.findById("");
    }

    @Test
    public void findByLoginTest() {
        final User user = userService.create("user", "test", "Ivan", "Ivanov");
        assertEquals(user, userService.findByLogin("user"));
    }

    @Test(expected = EmptyLoginException.class)
    public void findByLoginTestEmptyLoginException() {
        userService.findByLogin("");
    }

    @Test
    public void removeByIdTest() {
        final User user = userService.create("user", "test", "Ivan", "Ivanov");
        final String userId = user.getId();
        userService.removeById(userId);
        assertFalse(userService.getEntity().contains(user));
    }

    @Test(expected = EmptyIdException.class)
    public void removeByIdTestEmptyIdException() {
        userService.removeById("");
    }

    @Test
    public void removeByLoginTest() {
        final User user = userService.create("user", "test", "Ivan", "Ivanov");
        userService.removeByLogin("user");
        assertFalse(userService.getEntity().contains(user));
    }

    @Test(expected = EmptyLoginException.class)
    public void removeByLoginTestEmptyLoginException() {
        userService.removeByLogin("");
    }

    @Test
    public void createTest() {
        final User user = userService.create("user", "test", "Ivan", "Ivanov");
        assertTrue(userService.getEntity().contains(user));
    }

    @Test(expected = EmptyLoginException.class)
    public void createTestEmptyLoginException() {
        userService.create("", "test", "Ivan", "Ivanov");
    }

    @Test(expected = EmptyPassException.class)
    public void createTestEmptyPasswordExceptionException() {
        userService.create("user", "", "Ivan", "Ivanov");
    }

    @Test
    public void createWithEmailTest() {
        final User user = userService.create("user", "test", "Ivan", "Ivanov", "user@mail.ru");
        assertTrue(userService.getEntity().contains(user));
    }

    @Test(expected = EmptyEmailException.class)
    public void createWithEmailTestEmptyEmailException() {
        userService.create("user", "test", "Ivan", "Ivanov", "");
    }

    @Test
    public void createWithRoleTest() {
        final User user = userService.create("user", "test", "Ivan", "Ivanov", Role.ADMIN);
        assertTrue(userService.getEntity().contains(user));
    }

    @Test
    public void changeEmailTest() {
        final User user = userService.create("user", "test", "Ivan", "Ivanov", "user@mail.ru");
        userService.updateMail(user.getId(), "newEmail@mail.ru");
        assertEquals(user.getEmail(), "newEmail@mail.ru");
    }

    @Test(expected = AccessDeniedException.class)
    public void changeEmailTestEmptyUserIdException() {
        userService.updateMail("", "newEmail@mail.ru");
    }

    @Test(expected = EmptyEmailException.class)
    public void changeEmailTestEmptyEmailException() {
        userService.updateMail("001", "");
    }

    @Test
    public void changePasswordTest() {
        final User user = userService.create("user", "test", "Ivan", "Ivanov");
        userService.updatePassword(user.getId(), "test", "newPassword");
        assertEquals(user.getPasswordHash(), HashUtil.salt("newPassword"));
    }

    @Test(expected = AccessDeniedException.class)
    public void changePasswordTestEmptyUserIdException() {
        userService.updatePassword("", "test", "newPassword");
    }

    @Test(expected = EmptyPassException.class)
    public void changePasswordTestEmptyOldPasswordException() {
        userService.updatePassword("001", "", "newPassword");
    }

    @Test(expected = EmptyPassException.class)
    public void changePasswordTestEmptyNewPasswordException() {
        userService.updatePassword("001", "test", "");
    }

    @Test
    public void lockUserByLoginTest() {
        final User user = userService.create("user", "test", "Ivan", "Ivanov");
        userService.lockUserByLogin("user");
        assertTrue(user.getLocked());
    }

    @Test(expected = EmptyLoginException.class)
    public void lockUserByLoginTestEmptyLoginException() {
        userService.lockUserByLogin("");
    }

    @Test
    public void unlockUserByLoginTest() {
        final User user = userService.create("user", "test", "Ivan", "Ivanov");
        userService.lockUserByLogin("user");
        assertTrue(user.getLocked());
        userService.unlockUserByLogin("user");
        assertFalse(user.getLocked());
    }

    @Test(expected = EmptyLoginException.class)
    public void unlockUserByLoginTestEmptyLoginException() {
        userService.unlockUserByLogin("");
    }

    @Test
    public void getListTest() {
        userService.create("user", "test", "Ivan", "Ivanov");
        assertNotNull(userService.getEntity());
    }

    @Test
    public void loadVarargsTest() {
        final User user1 = new User();
        final User user2 = new User();
        userService.load(user1, user2);
        assertNotNull(userService.getEntity());
    }

    @Test
    public void loadListTest() {
        final List<User> users = new ArrayList<>();
        final User user1 = new User();
        users.add(user1);
        final User user2 = new User();
        users.add(user2);
        userService.load(users);
        assertNotNull(userService.getEntity());
    }

    @Test
    public void clearWithoutUserIdTest() {
        final User user1 = new User();
        final User user2 = new User();
        userService.load(user1, user2);
        userService.clear();
        assertTrue(userService.getEntity().isEmpty());
    }
*/

}
