package ru.fedun.tm.exception.incorrect;

import ru.fedun.tm.exception.AbstractRuntimeException;

public class IncorrectStartDateException extends AbstractRuntimeException {

    private final static String message = "Error! Start date is incorrect...";

    public IncorrectStartDateException() {
        super(message);
    }
}
