package ru.fedun.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.dto.Result;
import ru.fedun.tm.dto.SessionDTO;

public interface ISessionEndpoint {

    @Nullable
    SessionDTO openSession(@NotNull String login, @NotNull String password) throws Exception;

    @NotNull
    Result closeSession(@NotNull SessionDTO session) throws Exception;

    @NotNull
    Result closeAllSessionsForUser(@NotNull SessionDTO session) throws Exception;

}
