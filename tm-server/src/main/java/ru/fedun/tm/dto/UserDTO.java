package ru.fedun.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.entity.User;
import ru.fedun.tm.enumerated.Role;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDTO extends AbstractEntityDTO implements Serializable {

    private static final long serialVersionUID = 1001L;

    @NotNull
    private String login = "";

    @NotNull
    private String passwordHash = "";

    @NotNull
    private String email = "";

    @NotNull
    private String firstName = "";

    @NotNull
    private String middleName = "";

    @NotNull
    private String lastName = "";

    @NotNull
    private Role role = Role.USER;

    @NotNull
    private Boolean locked = false;

    @Override
    public String toString() {
        return getLogin();
    }

    @NotNull
    public static UserDTO toDTO(@NotNull final User user) {
        return new UserDTO(user);
    }

    @NotNull
    public static List<UserDTO> toDTO(@NotNull final Collection<User> users) {
        if (users.isEmpty()) return Collections.emptyList();
        @NotNull final List<UserDTO> result = new ArrayList<>();
        for (@NotNull final User user: users) {
            result.add(new UserDTO(user));
        }
        return result;
    }

    public UserDTO(@NotNull final User user) {
        id = user.getId();
        login = user.getLogin();
        passwordHash = user.getPasswordHash();
        email = user.getEmail();
        firstName = user.getFirstName();
        middleName = user.getMiddleName();
        lastName = user.getLastName();
        role = user.getRole();
        locked = user.getLocked();
    }

}

