package ru.fedun.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.api.repository.IUserRepository;
import ru.fedun.tm.api.service.IUserService;
import ru.fedun.tm.api.service.ServiceLocator;
import ru.fedun.tm.dto.UserDTO;
import ru.fedun.tm.entity.User;
import ru.fedun.tm.enumerated.Role;
import ru.fedun.tm.exception.empty.*;
import ru.fedun.tm.repository.UserRepository;
import ru.fedun.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.List;

public final class UserService extends AbstractService<User> implements IUserService {

    public UserService(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public void persist(@NotNull final User user) {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.persist(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void merge(@NotNull final User user) {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.merge(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@NotNull User user) {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.remove(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public User findOneById(@NotNull final String id) throws Exception {
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        final User user = repository.findOneById(id);
        entityManager.close();
        return user;
    }

    @NotNull
    @Override
    public User findOneByLogin(@NotNull final String login) throws Exception {
        if (login.isEmpty()) throw new EmptyLoginException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        final User user = repository.findOneByLogin(login);
        entityManager.close();
        return user;
    }

    @NotNull
    @Override
    public List<UserDTO> findAll() {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        final List<User> users = repository.findAll();
        entityManager.close();
        return UserDTO.toDTO(users);
    }

    @Override
    public void removeById(@NotNull final String id) throws Exception {
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.removeOneById(id);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeByLogin(@NotNull final String login) throws Exception {
        if (login.isEmpty()) throw new EmptyLoginException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.removeOneByLogin(login);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAll() {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.removeAll();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void create(@NotNull final String login, @NotNull final String password) throws Exception {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (password.isEmpty()) throw new EmptyPassException();
        @NotNull final User user = new User();
        user.setLogin(login);
        @Nullable final String saltPass = HashUtil.salt(password);
        if (saltPass == null) return;
        user.setPasswordHash(saltPass);
        persist(user);
    }

    @Override
    public void create(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final String firstName,
            @NotNull final String lastName,
            @NotNull final String email
    ) throws Exception {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (password.isEmpty()) throw new EmptyPassException();
        if (firstName.isEmpty()) throw new EmptyFirstNameException();
        if (lastName.isEmpty()) throw new EmptyLastNameException();
        if (email.isEmpty()) throw new EmptyEmailException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);
        @Nullable final String saltPass = HashUtil.salt(password);
        if (saltPass == null) return;
        user.setPasswordHash(saltPass);
        persist(user);
    }

    @Override
    public void create(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final String firstName,
            @NotNull final String lastName,
            @NotNull final Role role
    ) throws Exception {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (password.isEmpty()) throw new EmptyPassException();
        if (firstName.isEmpty()) throw new EmptyFirstNameException();
        if (lastName.isEmpty()) throw new EmptyLastNameException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        @Nullable final String saltPass = HashUtil.salt(password);
        if (saltPass == null) return;
        user.setPasswordHash(saltPass);
        user.setRole(role);
        persist(user);
    }

    @NotNull
    @Override
    public Long count() {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        final Long count = repository.count();
        entityManager.close();
        return count;
    }

    @Override
    public void updatePassword(
            @NotNull final String userId,
            @NotNull final String newPassword
    ) throws Exception {
        if (userId.isEmpty()) throw new EmptyLoginException();
        if (newPassword.isEmpty()) throw new EmptyPassException();
        @Nullable final String saltPass = HashUtil.salt(newPassword);
        if (saltPass == null) return;
        final User user = findOneById(userId);
        user.setPasswordHash(saltPass);
        merge(user);
    }

    @Override
    public void updateMail(
            @NotNull final String userId,
            @NotNull final String newEmail
    ) throws Exception {
        if (userId.isEmpty()) throw new EmptyLoginException();
        if (newEmail.isEmpty()) throw new EmptyEmailException();
        final User user = findOneById(userId);
        user.setEmail(newEmail);
        merge(user);
    }

    @Override
    public void lockUserByLogin(@NotNull final String login) throws Exception {
        if (login.isEmpty()) throw new EmptyLoginException();
        final User user = findOneByLogin(login);
        user.setLocked(true);
        merge(user);
    }

    @Override
    public void unlockUserByLogin(@NotNull final String login) throws Exception {
        if (login.isEmpty()) throw new EmptyLoginException();
        final User user = findOneByLogin(login);
        user.setLocked(false);
        merge(user);
    }

}
