package ru.fedun.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.api.endpoint.ISessionEndpoint;
import ru.fedun.tm.api.service.ServiceLocator;
import ru.fedun.tm.dto.Fail;
import ru.fedun.tm.dto.Result;
import ru.fedun.tm.dto.SessionDTO;
import ru.fedun.tm.dto.Success;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class SessionEndpoint implements ISessionEndpoint {

    private ServiceLocator serviceLocator;

    public SessionEndpoint() {
    }

    public SessionEndpoint(@NotNull ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    @Override
    @WebMethod
    public SessionDTO openSession(
            @WebParam(name = "login", partName = "login") @NotNull final String login,
            @WebParam(name = "password", partName = "password") @NotNull final String password
    ) throws Exception {
        return serviceLocator.getSessionService().open(login, password);
    }

    @NotNull
    @Override
    @WebMethod
    public Result closeSession(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getSessionService().close(session);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result closeAllSessionsForUser(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getSessionService().closeAll(session);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

}
