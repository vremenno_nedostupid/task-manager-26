
package ru.fedun.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.fedun.tm.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Exception_QNAME = new QName("http://endpoint.tm.fedun.ru/", "Exception");
    private final static QName _ShowUserProfile_QNAME = new QName("http://endpoint.tm.fedun.ru/", "showUserProfile");
    private final static QName _ShowUserProfileResponse_QNAME = new QName("http://endpoint.tm.fedun.ru/", "showUserProfileResponse");
    private final static QName _UpdateMail_QNAME = new QName("http://endpoint.tm.fedun.ru/", "updateMail");
    private final static QName _UpdateMailResponse_QNAME = new QName("http://endpoint.tm.fedun.ru/", "updateMailResponse");
    private final static QName _UpdateUserPassword_QNAME = new QName("http://endpoint.tm.fedun.ru/", "updateUserPassword");
    private final static QName _UpdateUserPasswordResponse_QNAME = new QName("http://endpoint.tm.fedun.ru/", "updateUserPasswordResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.fedun.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link ShowUserProfile }
     * 
     */
    public ShowUserProfile createShowUserProfile() {
        return new ShowUserProfile();
    }

    /**
     * Create an instance of {@link ShowUserProfileResponse }
     * 
     */
    public ShowUserProfileResponse createShowUserProfileResponse() {
        return new ShowUserProfileResponse();
    }

    /**
     * Create an instance of {@link UpdateMail }
     * 
     */
    public UpdateMail createUpdateMail() {
        return new UpdateMail();
    }

    /**
     * Create an instance of {@link UpdateMailResponse }
     * 
     */
    public UpdateMailResponse createUpdateMailResponse() {
        return new UpdateMailResponse();
    }

    /**
     * Create an instance of {@link UpdateUserPassword }
     * 
     */
    public UpdateUserPassword createUpdateUserPassword() {
        return new UpdateUserPassword();
    }

    /**
     * Create an instance of {@link UpdateUserPasswordResponse }
     * 
     */
    public UpdateUserPasswordResponse createUpdateUserPasswordResponse() {
        return new UpdateUserPasswordResponse();
    }

    /**
     * Create an instance of {@link SessionDTO }
     * 
     */
    public SessionDTO createSessionDTO() {
        return new SessionDTO();
    }

    /**
     * Create an instance of {@link UserDTO }
     * 
     */
    public UserDTO createUserDTO() {
        return new UserDTO();
    }

    /**
     * Create an instance of {@link AbstractEntityDTO }
     * 
     */
    public AbstractEntityDTO createAbstractEntityDTO() {
        return new AbstractEntityDTO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.fedun.ru/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ShowUserProfile }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.fedun.ru/", name = "showUserProfile")
    public JAXBElement<ShowUserProfile> createShowUserProfile(ShowUserProfile value) {
        return new JAXBElement<ShowUserProfile>(_ShowUserProfile_QNAME, ShowUserProfile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ShowUserProfileResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.fedun.ru/", name = "showUserProfileResponse")
    public JAXBElement<ShowUserProfileResponse> createShowUserProfileResponse(ShowUserProfileResponse value) {
        return new JAXBElement<ShowUserProfileResponse>(_ShowUserProfileResponse_QNAME, ShowUserProfileResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateMail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.fedun.ru/", name = "updateMail")
    public JAXBElement<UpdateMail> createUpdateMail(UpdateMail value) {
        return new JAXBElement<UpdateMail>(_UpdateMail_QNAME, UpdateMail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateMailResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.fedun.ru/", name = "updateMailResponse")
    public JAXBElement<UpdateMailResponse> createUpdateMailResponse(UpdateMailResponse value) {
        return new JAXBElement<UpdateMailResponse>(_UpdateMailResponse_QNAME, UpdateMailResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserPassword }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.fedun.ru/", name = "updateUserPassword")
    public JAXBElement<UpdateUserPassword> createUpdateUserPassword(UpdateUserPassword value) {
        return new JAXBElement<UpdateUserPassword>(_UpdateUserPassword_QNAME, UpdateUserPassword.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserPasswordResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.fedun.ru/", name = "updateUserPasswordResponse")
    public JAXBElement<UpdateUserPasswordResponse> createUpdateUserPasswordResponse(UpdateUserPasswordResponse value) {
        return new JAXBElement<UpdateUserPasswordResponse>(_UpdateUserPasswordResponse_QNAME, UpdateUserPasswordResponse.class, null, value);
    }

}
