package ru.fedun.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.endpoint.Session;
import ru.fedun.tm.endpoint.SessionDTO;

public interface ISessionService {

    @NotNull
    SessionDTO getCurrentSession();

    void setCurrentSession(@NotNull SessionDTO currentSession);

    void clearCurrentSession();

}

