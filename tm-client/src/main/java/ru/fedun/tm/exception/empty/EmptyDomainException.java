package ru.fedun.tm.exception.empty;

import ru.fedun.tm.exception.AbstractRuntimeException;

public class EmptyDomainException extends AbstractRuntimeException {

    private final static String message = "Error! Domain is empty...";

    public EmptyDomainException() {
        super(message);
    }

}
