package ru.fedun.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.bootstrap.Bootstrap;
import ru.fedun.tm.command.AbstractCommand;

import java.util.Collection;

public final class HelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-h";
    }

    @NotNull
    @Override
    public String name() {
        return "help";
    }

    @NotNull
    @Override
    public String description() {
        return "Display list of terminal commands.";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @NotNull final Bootstrap bootstrap = (Bootstrap) serviceLocator;
        @NotNull final Collection<AbstractCommand> commands = bootstrap.getCommands();
        for (@NotNull AbstractCommand command : commands) {
            System.out.println(command.name() + "- " + command.description());
        }
        System.out.println("[OK]");
        System.out.println();
    }

}
