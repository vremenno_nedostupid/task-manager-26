package ru.fedun.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.endpoint.*;

import java.lang.Exception;

public final class ProfileShowCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "profile";
    }

    @NotNull
    @Override
    public String description() {
        return "Show profile info.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SHOW PROFILE INFO]");
        @NotNull final SessionDTO session = serviceLocator.getSessionService().getCurrentSession();
        @NotNull final UserDTO user = serviceLocator.getUserEndpoint().showUserProfile(session);
        @NotNull final String login = user.getLogin();
        if (login.isEmpty()) System.out.println("Login: Unknown");
        else System.out.println("Login: " + login);

        @NotNull final String firstName = user.getFirstName();
        if (firstName.isEmpty()) System.out.println("First name: Unknown");
        else System.out.println("First Name: " + firstName);

        @NotNull final String secondName = user.getLastName();
        if (secondName.isEmpty()) System.out.println("Second name: Unknown");
        else System.out.println("Second Name: " + secondName);

        @NotNull final String email = user.getEmail();
        if (email.isEmpty()) System.out.println("E-mail: Unknown");
        else System.out.println("E-mail: " + email);

        @NotNull final Role role = user.getRole();
        System.out.println("Role " + role);
        System.out.println("[OK]");
        System.out.println();
    }

}
