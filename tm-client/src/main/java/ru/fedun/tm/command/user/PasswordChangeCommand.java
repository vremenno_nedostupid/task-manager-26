package ru.fedun.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.endpoint.SessionDTO;
import ru.fedun.tm.util.TerminalUtil;

public final class PasswordChangeCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "change-password";
    }

    @NotNull
    @Override
    public String description() {
        return "Change profile password.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("[ENTER OLD PASSWORD:]");
        @Nullable final String oldPassword = TerminalUtil.nextLine();
        System.out.println("[ENTER NEW PASSWORD:]");
        @Nullable final String newPassword = TerminalUtil.nextLine();
        @NotNull final SessionDTO session = serviceLocator.getSessionService().getCurrentSession();
        serviceLocator.getUserEndpoint().updateUserPassword(session, oldPassword, newPassword);
        System.out.println("[OK]");
        System.out.println();
    }

}
