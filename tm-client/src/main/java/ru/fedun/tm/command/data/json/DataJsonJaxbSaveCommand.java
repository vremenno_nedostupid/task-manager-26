package ru.fedun.tm.command.data.json;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.endpoint.Session;
import ru.fedun.tm.endpoint.SessionDTO;

public final class DataJsonJaxbSaveCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-save-json-jb";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to json (jax-b) file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON (JAX-B) SAVE]");
        @NotNull final SessionDTO session = serviceLocator.getSessionService().getCurrentSession();
        serviceLocator.getDataEndpoint().saveJsonByJaxb(session);
        System.out.println("[OK]");
        System.out.println();
    }

}
