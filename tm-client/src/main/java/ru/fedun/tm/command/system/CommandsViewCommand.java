package ru.fedun.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.command.AbstractCommand;

import java.util.List;

public final class CommandsViewCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-cmd";
    }

    @NotNull
    @Override
    public String name() {
        return "commands";
    }

    @NotNull
    @Override
    public String description() {
        return "Display program commands.";
    }

    @Override
    public void execute() {
        @NotNull final List<AbstractCommand> commands = serviceLocator.getCommandService().getCommands();
        for (@NotNull AbstractCommand command : commands) {
            System.out.println(command.name());
        }
    }

}
