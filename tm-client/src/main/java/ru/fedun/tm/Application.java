package ru.fedun.tm;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.bootstrap.Bootstrap;

public class Application {

    public static void main(String[] args) throws Exception {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
