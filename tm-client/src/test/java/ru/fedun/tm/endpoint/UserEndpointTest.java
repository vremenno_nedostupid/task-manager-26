package ru.fedun.tm.endpoint;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.fedun.tm.bootstrap.Bootstrap;
import ru.fedun.tm.marker.IntegrationClientCategory;

@Category(IntegrationClientCategory.class)
public class UserEndpointTest {
/*

    private final Bootstrap bootstrap = new Bootstrap();
    private Session userSession;

    @Before
    public void initSession() throws Exception_Exception {
        userSession = bootstrap.getSessionEndpoint().openSession("test", "test");
    }

    @Test
    public void createUserTest() throws Exception_Exception {
        final User user1 = bootstrap.getAdminEndpoint().createUserWithEmail("newUser", "pass", "ivan", "Ivan", "email");
        final Session session = bootstrap.getSessionEndpoint().openSession("newUser", "pass");
        Assert.assertEquals(user1.getLogin(), bootstrap.getUserEndpoint().showUserProfile(session).getLogin());
        Assert.assertEquals(user1.getPasswordHash(), bootstrap.getUserEndpoint().showUserProfile(session).getPasswordHash());
        bootstrap.getSessionEndpoint().closeSession(session);
        bootstrap.getAdminEndpoint().removeUserByLogin(bootstrap.getSessionEndpoint().openSession("admin", "admin"), "newUser");
    }

    @Test
    public void profileTest() throws Exception_Exception {
        final User user1 = bootstrap.getAdminEndpoint().createUserWithEmail("newUser", "pass", "ivan", "Ivan", "email");
        final Session session = bootstrap.getSessionEndpoint().openSession("newUser", "pass");
        Assert.assertEquals(user1.getLogin(), bootstrap.getUserEndpoint().showUserProfile(session).getLogin());
        Assert.assertEquals(user1.getPasswordHash(), bootstrap.getUserEndpoint().showUserProfile(session).getPasswordHash());
        Assert.assertEquals(user1.getEmail(), bootstrap.getUserEndpoint().showUserProfile(session).getEmail());
        Assert.assertEquals(user1.getFirstName(), bootstrap.getUserEndpoint().showUserProfile(session).getFirstName());
        Assert.assertEquals(user1.getSecondName(), bootstrap.getUserEndpoint().showUserProfile(session).getSecondName());
        Assert.assertEquals(user1.getRole(), bootstrap.getUserEndpoint().showUserProfile(session).getRole());
        bootstrap.getSessionEndpoint().closeSession(session);
        bootstrap.getAdminEndpoint().removeUserByLogin(bootstrap.getSessionEndpoint().openSession("admin", "admin"), "newUser");
    }

    @Test
    public void changeEmailTest() throws Exception_Exception {
        bootstrap.getUserEndpoint().updateUserEmail(userSession, "email");
        Assert.assertEquals(bootstrap.getUserEndpoint().showUserProfile(userSession).getEmail(), "email");
    }

    @Test
    public void changePasswordTest() throws Exception_Exception {
        bootstrap.getUserEndpoint().updateUserPassword(userSession, "test", "newPass");
        Assert.assertNotNull(bootstrap.getSessionEndpoint().openSession("test", "newPass"));
        bootstrap.getUserEndpoint().updateUserPassword(userSession, "newPass", "test");
    }
*/

}
